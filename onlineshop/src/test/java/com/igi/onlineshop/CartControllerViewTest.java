package com.igi.onlineshop;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;


@SpringBootTest
@AutoConfigureMockMvc
public class CartControllerViewTest {

	@Autowired
    private MockMvc mockMvc;

    @Test
    public void testCart() throws Exception{
    	
    	mockMvc.perform(get("/cart")).andExpect(status().isOk()).andExpect(view().name("cart"));
    	
     }
    
    }
	