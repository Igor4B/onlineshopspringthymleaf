package com.igi.onlineshop;



import org.junit.jupiter.api.Test;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
public class IndexControllerViewTest {
	
	@Autowired
    private MockMvc mockMvc;

    @Test
    public void testIndex() throws Exception{
    	
    	mockMvc.perform(get("/")).andExpect(status().isOk()).andExpect(view().name("index"));
    	
     }
    }
	
	
	


