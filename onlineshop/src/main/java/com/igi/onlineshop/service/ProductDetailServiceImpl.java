package com.igi.onlineshop.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.igi.onlineshop.entity.Product;
import com.igi.onlineshop.repository.ProductDetailRepository;

@Service
public class ProductDetailServiceImpl implements ProductDetailService{
	
	@Autowired
	private ProductDetailRepository productDetailRepository;

	

	@Override
	public Optional<Product> findById(Integer id) {
		return productDetailRepository.findById(id);
	}

	

	

}
