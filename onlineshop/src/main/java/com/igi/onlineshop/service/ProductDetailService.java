package com.igi.onlineshop.service;

import java.util.Optional;

import com.igi.onlineshop.entity.Product;

public interface ProductDetailService {
	
	 Optional<Product> findById(Integer id);

}
