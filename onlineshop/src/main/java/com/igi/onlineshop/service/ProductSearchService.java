package com.igi.onlineshop.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.igi.onlineshop.entity.Product;
import com.igi.onlineshop.repository.ProductSearchRepository;

@Service
public class ProductSearchService {
	
	@Autowired
	private ProductSearchRepository productSearchRepository;
	
	
	public List<Product> findByKeyword(String keyword){
		return productSearchRepository.findByKeyword(keyword);
		
	}
	

}
