package com.igi.onlineshop;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OnlineShopSpringThymeleafApplication {

	public static void main(String[] args) {
		SpringApplication.run(OnlineShopSpringThymeleafApplication.class, args);
	}

}
