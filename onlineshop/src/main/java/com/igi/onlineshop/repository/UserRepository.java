package com.igi.onlineshop.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.igi.onlineshop.entity.User;

public interface UserRepository extends JpaRepository<User, Integer> {
	User findByUsername(String username);
}
