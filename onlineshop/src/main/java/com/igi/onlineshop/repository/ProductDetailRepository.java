package com.igi.onlineshop.repository;

import org.springframework.data.jpa.repository.JpaRepository;


import com.igi.onlineshop.entity.Product;

public interface ProductDetailRepository extends JpaRepository<Product, Integer>  {

	Product findById(Long id);

}
