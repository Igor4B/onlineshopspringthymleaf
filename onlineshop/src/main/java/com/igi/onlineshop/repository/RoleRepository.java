package com.igi.onlineshop.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.igi.onlineshop.entity.Role;

public interface RoleRepository extends JpaRepository<Role, String> {

}
