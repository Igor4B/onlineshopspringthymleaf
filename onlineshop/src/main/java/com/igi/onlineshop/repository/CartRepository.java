package com.igi.onlineshop.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.igi.onlineshop.entity.Cart;
import com.igi.onlineshop.entity.User;

public interface CartRepository extends JpaRepository<Cart, Integer> {
	List<Cart> findByUser( User user);
	
}
