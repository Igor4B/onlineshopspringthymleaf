package com.igi.onlineshop.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.igi.onlineshop.entity.Product;

@Repository
public interface ProductPagingAndSearchRepository extends PagingAndSortingRepository<Product, Integer> {

	
	
}
