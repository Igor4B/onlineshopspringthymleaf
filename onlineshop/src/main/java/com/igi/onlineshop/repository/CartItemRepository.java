package com.igi.onlineshop.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.igi.onlineshop.entity.Cart;
import com.igi.onlineshop.entity.CartItem;

public interface CartItemRepository extends JpaRepository<CartItem, Integer> {
	List<CartItem> findByCart(Cart cart);

}
