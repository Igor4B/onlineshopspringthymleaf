package com.igi.onlineshop.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.igi.onlineshop.entity.Product;

public interface ProductRepository extends JpaRepository<Product, Integer> {

	
}
