package com.igi.onlineshop.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.igi.onlineshop.entity.Product;

@Repository
public interface ProductSearchRepository extends JpaRepository<Product, Integer> {
	
	@Query(value = "select * from Products p where p.Name like %:keyword%",nativeQuery=true)
	List<Product> findByKeyword(@Param("keyword") String keyword);
	

}
