package com.igi.onlineshop.utils;



import java.util.List;

import com.igi.onlineshop.entity.Cart;
import com.igi.onlineshop.entity.CartItem;

public class OrderContainer {
    private Cart cart;
    private List<CartItem> items;

    public OrderContainer(Cart cart, List<CartItem> items) {
        this.cart = cart;
        this.items = items;
    }

    public Integer getTotalPrice(){
        int result = 0;
        for (CartItem cartItem : items){
            result += cartItem.getTotalPrice();
        }
        return result;
    }

    public Cart getCart() {
        return cart;
    }

    public List<CartItem> getItems() {
        return items;
    }

}
