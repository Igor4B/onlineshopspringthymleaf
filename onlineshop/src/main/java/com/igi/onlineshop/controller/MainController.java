package com.igi.onlineshop.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.igi.onlineshop.entity.Product;
import com.igi.onlineshop.repository.ProductPagingAndSearchRepository;
import com.igi.onlineshop.service.ProductDetailService;
import com.igi.onlineshop.service.ProductSearchService;
import com.igi.onlineshop.service.ProductService;
import com.igi.onlineshop.utils.CartContainer;

@Controller
public class MainController {
	@Autowired
	private ProductService productService;
	
	@Autowired ProductSearchService productSearchService;
	
	@Autowired
	private ProductDetailService productDetailService;

	@Autowired
	private ProductPagingAndSearchRepository productpagingRepository;

	@Autowired
	private CartContainer cartContainer;
	

	@RequestMapping("/")
	public String indexPage(@PageableDefault(size = 6) Pageable pageable, Model model) {
		Page<Product> page = productpagingRepository.findAll(pageable);	
		model.addAttribute("page", page);
		return "index";
	}
	
	@RequestMapping("/showProduct")
	public String showProducts(Model model,String keyword){
		
		model.addAttribute("oneproduct", productSearchService.findByKeyword(keyword) );
		
		return "showProduct";
		
	}

	@RequestMapping("/cart/add/{id}")
	public String cartAddProduct(@PathVariable(value = "id") int id) {
		cartContainer.addItem(productService.getProduct(id));
		System.out.println(cartContainer);
		return "redirect:/";
	}

	@RequestMapping("/cart/remove/{id}")
	public String cartRemoveProduct(@PathVariable(value = "id") int id) {
		cartContainer.removeItem(id);
		return "redirect:/cart";
	}

	@RequestMapping("/cart/clear")
	public String cartClear() {
		cartContainer.clear();
		return "redirect:/cart";
	}

	@RequestMapping("/cart")
	public String cartView() {
		return "cart";
	}

	@RequestMapping("/about")
	public String aboutCompany() {
		return "about";
	}

	@RequestMapping(value = "/productDetail{product_id}", method = RequestMethod.GET)
	public String carDetail(Model model, @RequestParam(value = "product_id") Integer productId) {
		Optional<Product> productById = productDetailService.findById(productId);
		if (productById.isPresent()) {
			model.addAttribute("productById", productById.get());
		}
		return "productDetail";
	}
}

